<?php

namespace Drupal\create_form\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class FormPageController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function myPage() {

    $config = \Drupal::config('create_form.settings');
    $param = $config->get('title');
    $element = [
      '#markup' => $param,
    ];
    return $element;
  }

}
