<?php

namespace Drupal\create_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class CreateFormSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'create_form.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('create_form.settings');

    $form['form_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('form_title'),
    ];

    $form['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Form Elements'),
      '#description' => $this->t('Enter form elements as key_name=>field_type. New field on new line'),
      '#placeholder' => $this->t('name=>type'),
      '#default_value' => $config->get('body'),
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $config->get('email'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('create_form.settings')
      // Set the submitted configuration setting.
      ->set('form_title', $form_state->getValue('form_title'))
      // You can set multiple configurations at once by making
      // multiple calls to set()
      ->set('body', $form_state->getValue('body'))
      ->set('email', $form_state->getValue('email'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
