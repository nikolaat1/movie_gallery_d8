<?php

namespace Drupal\create_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\email_event\EmailEvent;

/**
 *
 */
class FormPage extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'form_page';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('create_form.settings');
    $title = $config->get('form_title');
    $body = $config->get('body');
    $email = $config->get('email');

    $form['form_title'] = [
      '#type' => 'markup',
      "#markup" => "<h2>" . $title . "</h2>",

    ];
    $bodyParams = explode("\r\n", $body);
    foreach ($bodyParams as $param) {
      $subparams = explode('=>', $param);
      if (count($subparams) === 2) {
        list($key, $fieldType) = $subparams;
        if (isset($key) && isset($fieldType)) {
          $form[$key] = [
            '#type' => $fieldType,
            '#title' => ucfirst($this->t($key)),
          ];
        }
      }
    }
    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Email',
      '#default_value' => $email,
      '#disabled' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send!'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $dispatcher = \Drupal::service('event_dispatcher');
    $module = 'create_form';
    $key = 'send_custom_email';
    $to = $form_state->getValue('email');
    $params['subject'] = $form_state->getValue('subject');
    $event = new EmailEvent();
    $dispatcher->dispatch(EmailEvent::SEND, $event);
    $params['body'] = $form_state->getValue('body');


    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
    }
    else {
      drupal_set_message(t('Your message has been sent.'));
    }
  }

}
