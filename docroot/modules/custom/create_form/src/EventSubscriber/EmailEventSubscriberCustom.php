<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 27.06.18
 * Time: 17:42
 */

namespace Drupal\create_form\EventSubscriber;

use Drupal\email_event\EmailEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class EmailEventSubscriberCustom implements EventSubscriberInterface
{

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * array('eventName' => 'methodName')
   *  * array('eventName' => array('methodName', $priority))
   *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
   *
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents() {
    $events[EmailEvent::SEND][]=['extendEmailName', 800];
    return $events;
  }

  public function extendEmailName(EmailEvent $event){
    $emailStart = $event->getMailStart();
    $emailEnd = '@abv.com';
    drupal_set_message('Email should be send to :::'.$emailStart.$emailEnd);
  }
}
