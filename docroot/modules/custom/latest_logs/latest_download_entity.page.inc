<?php

/**
 * @file
 * Contains latest_download_entity.page.inc.
 *
 * Page callback for Latest download entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Latest download entity templates.
 *
 * Default template: latest_download_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_latest_download_entity(array &$variables) {
  // Fetch LatestDownloadEntity Entity Object.
  $latest_download_entity = $variables['elements']['#latest_download_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
