<?php

namespace Drupal\latest_logs\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Latest download entity entities.
 *
 * @ingroup latest_logs
 */
class LatestDownloadEntityDeleteForm extends ContentEntityDeleteForm {


}
