<?php

namespace Drupal\latest_logs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Latest download entity entities.
 *
 * @ingroup latest_logs
 */
class LatestDownloadEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Latest download entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\latest_logs\Entity\LatestDownloadEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.latest_download_entity.edit_form',
      ['latest_download_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
