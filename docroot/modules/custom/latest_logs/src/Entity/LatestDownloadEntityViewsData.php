<?php

namespace Drupal\latest_logs\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Latest download entity entities.
 */
class LatestDownloadEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
