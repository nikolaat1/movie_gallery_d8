<?php

namespace Drupal\latest_logs\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Latest download entity entities.
 *
 * @ingroup latest_logs
 */
interface LatestDownloadEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Latest download entity name.
   *
   * @return string
   *   Name of the Latest download entity.
   */
  public function getName();

  /**
   * Sets the Latest download entity name.
   *
   * @param string $name
   *   The Latest download entity name.
   *
   * @return \Drupal\latest_logs\Entity\LatestDownloadEntityInterface
   *   The called Latest download entity entity.
   */
  public function setName($name);

  /**
   * Gets the Latest download entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Latest download entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Latest download entity creation timestamp.
   *
   * @param int $timestamp
   *   The Latest download entity creation timestamp.
   *
   * @return \Drupal\latest_logs\Entity\LatestDownloadEntityInterface
   *   The called Latest download entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Latest download entity published status indicator.
   *
   * Unpublished Latest download entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Latest download entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Latest download entity.
   *
   * @param bool $published
   *   TRUE to set this Latest download entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\latest_logs\Entity\LatestDownloadEntityInterface
   *   The called Latest download entity entity.
   */
  public function setPublished($published);

}
