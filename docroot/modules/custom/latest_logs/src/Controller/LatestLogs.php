<?php
/**
 * Created by PhpStorm.
 * User: natanasov
 * Date: 03.07.18
 * Time: 13:21
 */

namespace Drupal\latest_logs\Controller;

use Drupal\Core\Controller\ControllerBase;

class LatestLogs extends ControllerBase {
  public function myPage() {
    $element = array(
      '#markup' => 'Hello, world',
    );
    return $element;
  }
}