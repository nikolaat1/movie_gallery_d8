<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 28.06.18
 * Time: 10:43
 */

namespace Drupal\email_subsc\EventSubscriber;


use Drupal\email_event\EmailEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailSubsc implements EventSubscriberInterface{

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * array('eventName' => 'methodName')
   *  * array('eventName' => array('methodName', $priority))
   *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
   *
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents() {
    $events[EmailEvent::SEND][] = 'addCC';
    return $events;
  }

  public function addCC(EmailEvent $event){
    $params = $event->getParams();
    $params['cc'] = 'ivan40@bv.gb';
    $event->setParams($params);
  }
}
