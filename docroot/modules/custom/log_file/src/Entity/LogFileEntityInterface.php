<?php

namespace Drupal\log_file\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Log file entities.
 *
 * @ingroup log_file
 */
interface LogFileEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Log file name.
   *
   * @return string
   *   Name of the Log file.
   */
  public function getName();

  /**
   * Sets the Log file name.
   *
   * @param string $name
   *   The Log file name.
   *
   * @return \Drupal\log_file\Entity\LogFileEntityInterface
   *   The called Log file entity.
   */
  public function setName($name);

  /**
   * Gets the Log file creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Log file.
   */
  public function getCreatedTime();

  /**
   * Sets the Log file creation timestamp.
   *
   * @param int $timestamp
   *   The Log file creation timestamp.
   *
   * @return \Drupal\log_file\Entity\LogFileEntityInterface
   *   The called Log file entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Log file published status indicator.
   *
   * Unpublished Log file are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Log file is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Log file.
   *
   * @param bool $published
   *   TRUE to set this Log file to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\log_file\Entity\LogFileEntityInterface
   *   The called Log file entity.
   */
  public function setPublished($published);

}
