<?php

namespace Drupal\log_file\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Log file entity.
 *
 * @ingroup log_file
 *
 * @ContentEntityType(
 *   id = "log_file",
 *   label = @Translation("Log file"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\log_file\LogFileEntityListBuilder",
 *     "views_data" = "Drupal\log_file\Entity\LogFileEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\log_file\Form\LogFileEntityForm",
 *       "add" = "Drupal\log_file\Form\LogFileEntityForm",
 *       "edit" = "Drupal\log_file\Form\LogFileEntityForm",
 *       "delete" = "Drupal\log_file\Form\LogFileEntityDeleteForm",
 *     },
 *     "access" = "Drupal\log_file\LogFileEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\log_file\LogFileEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "log_file",
 *   admin_permission = "administer log file entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "file" = "file"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/log_file/{log_file}",
 *     "add-form" = "/admin/structure/log_file/add",
 *     "edit-form" = "/admin/structure/log_file/{log_file}/edit",
 *     "delete-form" = "/admin/structure/log_file/{log_file}/delete",
 *     "collection" = "/admin/structure/log_file",
 *   },
 *   field_ui_base_route = "log_file.settings"
 * )
 */
class LogFileEntity extends ContentEntityBase implements LogFileEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->get('file')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Log file entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Log file entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Log file is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));
    $validators = array(
      'file_validate_extensions' => array('csv'),
    );
    $fields['file'] = BaseFieldDefinition::create('file')
      ->setLabel(t('File name'))
      ->setDescription('File with logs')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'managed_file',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'managed_file',
        'weight' => 5,
      ])
      ->setSettings([
        'file_extensions' => 'csv',
        'upload_location' => 'public://my_upload'
      ]);


    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
