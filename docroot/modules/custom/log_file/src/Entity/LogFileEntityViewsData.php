<?php

namespace Drupal\log_file\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Log file entities.
 */
class LogFileEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
