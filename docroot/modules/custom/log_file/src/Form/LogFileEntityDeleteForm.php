<?php

namespace Drupal\log_file\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Log file entities.
 *
 * @ingroup log_file
 */
class LogFileEntityDeleteForm extends ContentEntityDeleteForm {


}
