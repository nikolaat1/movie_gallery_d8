<?php

namespace Drupal\log_file;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Log file entities.
 *
 * @ingroup log_file
 */
class LogFileEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Log file ID');
    $header['name'] = $this->t('Name');
    $header['file'] = $this->t('File Id');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\log_file\Entity\LogFileEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.log_file.edit_form',
      ['log_file' => $entity->id()]
    );
    $file = $entity->get('file')->getValue();
    $fileId = $file[0]['target_id'];
    if ($fileId == null){
      $fileId = 'No file';
    }
    $row['file'] = Link::createFromRoute(
      $fileId,
      'entity.log_file.edit_form',
      ['log_file' => $entity->id()]
    );

    $v=4;
    return $row + parent::buildRow($entity);
  }
}