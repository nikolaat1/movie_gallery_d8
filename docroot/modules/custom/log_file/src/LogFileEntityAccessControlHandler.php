<?php

namespace Drupal\log_file;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Log file entity.
 *
 * @see \Drupal\log_file\Entity\LogFileEntity.
 */
class LogFileEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\log_file\Entity\LogFileEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished log file entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published log file entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit log file entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete log file entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add log file entities');
  }

}
