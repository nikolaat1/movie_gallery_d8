<?php

/**
 * @file
 * Contains log_file.page.inc.
 *
 * Page callback for Log file entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Log file templates.
 *
 * Default template: log_file.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_log_file(array &$variables) {
  // Fetch LogFileEntity Entity Object.
  $log_file = $variables['elements']['#log_file'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
