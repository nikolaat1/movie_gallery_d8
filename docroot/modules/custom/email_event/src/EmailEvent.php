<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 27.06.18
 * Time: 17:35
 */

namespace Drupal\email_event;

use Symfony\Component\EventDispatcher\Event;




class EmailEvent extends Event {
  const SEND = 'event.send';

  private $params;
  function __construct($params) {
    $this->params = $params;
  }

  public function getParams() {
    return $this->params;
  }

  public function setParams($params) {
    $this->params = $params;
  }
}
