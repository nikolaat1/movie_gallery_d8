<?php

namespace Drupal\email_event\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\email_event\EmailEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class EventForm extends FormBase{

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'event_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => 'Subject',
    ];

    $form['body'] = [
      '#type' => 'textfield',
      '#title' => 'Body',
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Send to'
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send!'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'email_event';
    $key = 'send_email';
    $to = $form_state->getValue('email');
    $params['subject'] = $form_state->getValue('subject');
    $params['body'] = $form_state->getValue('body');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = true;

    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event = new EmailEvent($params);
    $event_dispatcher->dispatch(EmailEvent::SEND, $event);

    $params = $event->getParams();
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
       drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
    }
    else {
      drupal_set_message(t('Your message has been sent.'));
    }
  }
}
