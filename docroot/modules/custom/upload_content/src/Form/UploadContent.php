<?php
/**
 * Created by PhpStorm.
 * User: nikola
 * Date: 24.06.18
 * Time: 12:42
 */

namespace Drupal\upload_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use \Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

class UploadContent extends FormBase{

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'form_content';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $validators = array(
      'file_validate_extensions' => array('txt'),
    );
    $form['upload_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('File'),
      '#upload_location' => 'public://my_upload',
      '#upload_validators' => $validators,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $fid = $form_state->getValue('upload_file');
    $file = File::load(reset($fid));
    $fileUri = $file->getFileUri();
    $file = fopen($file->getFileUri(), 'r');
    $content = fread($file, filesize($fileUri));

    //Cast to JSON object
    $jsContent = json_decode($content);

    //Download picture from INTERNET based on URL
    if (isset($jsContent->{'field_image_postert'})){
      $pictureUrl = $jsContent->{'field_image_postert'};
      $result = system_retrieve_file($pictureUrl, $destination = "public://pictures", $managed = TRUE, $replace = FILE_EXISTS_REPLACE);
    }


    if (isset($jsContent->{'field_genre'})){
      $jsonTerm = $jsContent->{'field_genre'};
      $jsonTermId = $this->checkTermExist($jsonTerm);

    }



    //Create Node
    $node = Node::create([
      'type' => 'film',
      'title' => $jsContent->{'title'},
      'body' => $jsContent->{'body'},
      'field_trailer' => isset($jsContent->{'field_trailer'}) ? $jsContent->{'field_trailer'} : '',
      'field_image_postert'=> isset($result) ? $result : null,
      'field_genre'=>isset($jsonTermId) ? [
        ['target_id' => $jsonTermId]
      ] : null,
      'field_link_imdb' => isset($jsContent->{'field_link_imdb'}) ? $jsContent->{'field_link_imdb'} : '',
      'field_realease_date' => isset($jsContent->{'field_realease_date'}) ? $jsContent->{'field_realease_date'} : '',
    ]);

    $node->save();

    fclose($file);

  }

  public function checkTermExist($jsonTerm) {
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('genre');
    foreach ($terms as $term){
      $id = $term->tid;
      $name = $term->name;
      if ($jsonTerm == $name){
        $jsonTermId = $id;
      }
    }
    if (!isset($jsonTermId)){
      $newTermName = $jsonTerm;
      $new_term = \Drupal\taxonomy\Entity\Term::create([
        'vid' => 'genre',
        'name' => $newTermName,
      ]);
      $new_term->enforceIsNew();
      $new_term->save();
      $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('genre');
      foreach ($terms as $term){
        $id = $term->tid;
        $name = $term->name;
        if ($jsonTerm == $name){
          $jsonTermId = $id;
        }
      }
    }
    return $jsonTermId;
  }
}
